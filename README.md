# Cursor^0.0.5 ^[wiki](https://gitlab.com/ancient/cursor/wikis/home)

Unified operations with data sets based on single adapter.

## Theory

Not tied to a particular data structure.

It works only with universal operations.

###### `object` adapters

It is a object with user adapted methods of any of databases for some class.

A detailed description of the requirements to the adapter can be found in the [wiki](https://gitlab.com/ancient/document/wikis/home).

[Adapters repository](https://gitlab.com/ancient/adapters).

## Example

```js
var Adapters = require('ancient-adapters').Adapters;
var Refs = require('ancient-refs');
var DocumentsFactory = require('ancient-document').Factory;
var Cursor = require('ancient-cursor').Cursor;
var adapterAutoComplete = require('ancient-cursor').adapterAutoComplete;

// In this example, the pointer to the data in the cursor is the function, but it depends on the adapter.
var demoStorage = {
  a: { _id: 'a' },
  b: { _id: 'b' },
  c: { _id: 'c' },
  d: { _id: 'd' }
};

var adapters = new Adapters();
var adapterTest = {

  // (id: String, callback: (error?, result?: any))
  getRef: function(id, callback) {
    callback(undefined, demoStorage[id]);
  },
  
  // (data: any) => id: String
  newRef: function(data) {
    return data._id;
  },
  
  // (id: String, callback: (error?, result?: Boolean))
  deleteRef: function(id, callback) {
    if (demoStorage[id]) {
      delete demoStorage[id];
      if (callback) callback(undefined, true);
    } else {
      if (callback) callback(undefined, undefined);
    }
  },
  
  // (pointer: any, callback: (error?, results?: [any])
  cursorFetch: function(pointer, callback) {
    var results = [];
    for (var id in demoStorage) {
      if (pointer(demoStorage[id])) results.push(demoStorage[id]);
    }
    callback(undefined, results);
  },
  
  // (pointer: any, handler: (error?, results?: [any], callback: ())
  cursorEach: function(pointer, handler, callback) {
    for (var id in demoStorage) {
      if (pointer(demoStorage[id])) handler(undefined, demoStorage[id]);
    }
    callback(undefined);
  }
};

// AutoComplete methods in adapter.
// You can specify only one of the following options:
// cursorFetch or cursorEach or (cursorCount and cursorSlice)
// (adapter: Adapter) => adapter: Adapter|thrown Error
adapterAutoComplete(adapterTest);
// However, it is recommended to specify all adapters for greater productivity

adapters.add('test', adapterTest);

var documentsFactory = new DocumentsFactory(adapters);
var refs = new Refs(documentsFactory);

var cursor = new Cursor(refs, 'test', function(data) { return data._id == 'a' || data._id == 'b'; });

cursor.fetch();
// [
// Document { data: { _id: 'a' } },
// Document { data: { _id: 'b' } }
// ]

cursor.each(function(document) {
  console.log(document);
}, function() {
  console.log('done');
});
// Document { data: { _id: 'a' } }
// Document { data: { _id: 'b' } }
// done

cursor.count();
// 2

cursor.slice(1, 1);
// [
// Document { data: { _id: 'b' } }
// ]

cursor.first();
// Document { data: { _id: 'b' } }
```

If the last argument to pass `true`, then data without Document capsule will be returned.

## Versions

### 0.0.5
* Move Cursor into exports value
* Add adapterAutoCompleter

### 0.0.4
* Add cursorFirst to adapter

### 0.0.3
* Remove first argument in all methods adapterName

### 0.0.2
* Slice and first methods.

### 0.0.1
* Support for new Adapters class.

### 0.0.0
* class Cursor