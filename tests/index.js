var assert = require('chai').assert;
var lodash = require('lodash');
var Adapters = require('ancient-adapters').Adapters;
var Refs = require('ancient-refs');
var Cursor = require('../index.js').Cursor;
var adapterAutoComplete = require('../index.js').adapterAutoComplete;
var test = require('./test.js');
var DocumentsFactory = require('ancient-document').Factory;

describe('ancient/cursor', function() {
  var storage = {
    a: { _id: 'a' },
    b: { _id: 'b' },
    c: { _id: 'c' },
    d: { _id: 'd' }
  };
  
  var adapters = new Adapters();
  var adapterTest = {
    
    // (id: String, callback: (error?, result?: any))
    getRef: function(id, callback) {
      if (callback) callback(undefined, storage[id]);
    },
    
    // (data: any) => id: String
    newRef: function(data) {
      return data._id;
    },
    
    // (id: String, callback: (error?, result?: Boolean))
    deleteRef: function(id, callback) {
      if (storage[id]) {
        delete storage[id];
        if (callback) callback(undefined, true);
      } else {
        if (callback) callback(undefined, undefined);
      }
    },
    
    // (pointer: any, callback: (error?, results?: [any])
    cursorFetch: function(pointer, callback) {
      var results = [];
      for (var id in storage) {
        if (pointer(storage[id])) results.push(storage[id]);
      }
      callback(undefined, results);
    },
    
    // (pointer: any, handler: (error?, results?: [any], callback: ())
    cursorEach: function(pointer, handler, callback) {
      for (var id in storage) {
        if (pointer(storage[id])) handler(undefined, storage[id]);
      }
      callback(undefined);
    }
  };
  adapters.add('test', adapterAutoComplete(adapterTest));
  
  var documentsFactory = new DocumentsFactory(adapters);
  var refs = new Refs(documentsFactory);
  var cursor = new Cursor(refs, 'test', function(data) { return data._id == 'a' || data._id == 'b'; });
  
  describe('test demo storage adapter', function() {
    test(cursor, [
      { _id: 'a' },
      { _id: 'b' }
    ], true, true);
  });
});