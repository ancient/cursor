var assert = require('chai').assert;
var Refs = require('ancient-refs');
var DocumentsFactory = require('ancient-document').Factory;
var lodash = require('lodash');

module.exports = function(cursor, requiredResults, allowAsync, allowSync) {
  if (allowAsync) {
    it('async fetch', function(done) {
      cursor.fetch(function(error, results) {
        
        // Each of results
        for (var r0 in results) {
          var finded = false;
          // Must be in requiredResults
          for (var r1 in requiredResults) {
            if (lodash.isEqual(requiredResults[r1], results[r0].data)) finded = true;
          }
          if (!finded) {
            throw new Error('Result '+JSON.stringify(results[r0].data)+' is not finded in requiredResults');
          }
        }
        
        // All of the required results should be exists
        assert.equal(results.length, requiredResults.length);
        
        done();
      });
    });
  }
  if (allowAsync) {
    it('async fetch onlyData', function(done) {
      cursor.fetch(function(error, results) {
        
        // Each of results
        for (var r0 in results) {
          var finded = false;
          // Must be in requiredResults
          for (var r1 in requiredResults) {
            if (lodash.isEqual(requiredResults[r1], results[r0])) finded = true;
          }
          if (!finded) {
            throw new Error('Result '+JSON.stringify(results[r0])+' is not finded in requiredResults');
          }
        }
        
        // All of the required results should be exists
        assert.equal(results.length, requiredResults.length);
        
        done();
      }, true);
    });
  }
  if (allowSync) {
    it('sync fetch onlyData', function() {
      var results = cursor.fetch(undefined, true);
      
      // Each of results
      for (var r0 in results) {
        var finded = false;
        // Must be in requiredResults
        for (var r1 in requiredResults) {
          if (lodash.isEqual(requiredResults[r1], results[r0])) finded = true;
        }
        if (!finded) {
          throw new Error('Result '+JSON.stringify(results[r0])+' is not finded in requiredResults');
        }
      }
      
      // All of the required results should be exists
      assert.equal(results.length, requiredResults.length);
    });
  }
  it('each', function(done) {
    cursor.each(function(error, document) {
      var finded = false;
      // Must be in requiredResults
      for (var r1 in requiredResults) {
        if (lodash.isEqual(requiredResults[r1], document.data)) finded = true;
      }
      if (!finded) {
        throw new Error('Result '+JSON.stringify(document.data)+' is not finded in requiredResults');
      }
    }, function() {
      done();
    });
  });
  it('each onlyData', function(done) {
    cursor.each(function(error, data) {
      var finded = false;
      // Must be in requiredResults
      for (var r1 in requiredResults) {
        if (lodash.isEqual(requiredResults[r1], data)) finded = true;
      }
      if (!finded) {
        throw new Error('Result '+JSON.stringify(data)+' is not finded in requiredResults');
      }
    }, function() {
      done();
    }, true);
  });
  if (allowAsync) {
    it('async count', function(done) {
      cursor.count(function(error, count) {
        assert.equal(count, requiredResults.length);
        done();
      });
    });
  }
  if (allowSync) {
    it('sync count', function() {
      assert.equal(cursor.count(), requiredResults.length);
    });
  }
  if (allowAsync) {
    it('async slice onlyData', function(done) {
      cursor.slice(0, 1, function(error, results) {
        assert.lengthOf(results, 1);
        assert.deepEqual(results[0], requiredResults[0]);
        
        done();
      }, true);
    });
    it('async slice', function(done) {
      cursor.slice(0, 1, function(error, results) {
        assert.lengthOf(results, 1);
        assert.deepEqual(results[0].data, requiredResults[0]);
        
        done();
      });
    });
  }
  if (allowSync) {
    it('sync slice onlyData', function(done) {
      var results = cursor.slice(0, 1, undefined, true);
      assert.lengthOf(results, 1);
      assert.deepEqual(results[0], requiredResults[0]);
      
      done();
    });
    it('sync slice', function(done) {
      var results = cursor.slice(0, 1);
      assert.lengthOf(results, 1);
      assert.deepEqual(results[0].data, requiredResults[0]);
      
      done();
    });
  }
};