/** Adapters requirements **/

// cursorFetch(pointer: any, callback: (error?, results?: [any]))
// cursorEach(pointer: any, handler: (error?, result: any), callback: Function)
// cursorCount(pointer: any, callback: (error?, count?: Number = 0))
// cursorSlice(pointer: any, begin: Number, length: Number, callback: (error?, results?: [any]))
// cursorFirst(pointer: any, callback: (error?, result?: any))

/** Cursor **/

// new (refs: Refs, adapterName: String, pointer: any)
var Cursor = function(refs, adapterName, pointer) {
  this.refs = refs;
  this.adapterName = adapterName;
  this.adapter = this.refs.adapters.get(this.adapterName);
  if (!this.adapter) {
    throw new Error('Adapter "'+this.adapterName+'" is not defined.');
  }
  this.pointer = pointer;
  this.adapterValidate();
};

// () => undefined|thrown Error
Cursor.prototype.adapterValidate = function() {
  var adapter = this.adapter;
  if (!adapter.cursorFetch) throw new Error('Adapter "'+adapter.adapterName+'" is incompatible with Cursor. Method "cursorFetch" is not defined.');
  if (!adapter.cursorEach) throw new Error('Adapter "'+adapter.adapterName+'" is incompatible with Cursor. Method "cursorEach" is not defined.');
  if (!adapter.cursorCount) throw new Error('Adapter "'+adapter.adapterName+'" is incompatible with Cursor. Method "cursorCount" is not defined.');
  if (!adapter.cursorSlice) throw new Error('Adapter "'+adapter.adapterName+'" is incompatible with Cursor. Method "cursorSlice" is not defined.');
  if (!adapter.cursorFirst) throw new Error('Adapter "'+adapter.adapterName+'" is incompatible with Cursor. Method "cursorFirst" is not defined.');
};

// (callback?: (error?, results?: [Document])) => results?: [Document]
// (callback?: (error?, results?: [Data]), onlyData: Boolean = true) => results?: [Data]
Cursor.prototype.fetch = function(callback, onlyData) {
  var cursor = this;
  var _results;
  this.adapter.cursorFetch(this.pointer, function(error, results) {
    _results = results;
    if (!onlyData) {
      for (var r in _results) {
        _results[r] = cursor.refs.documentsFactory.new(cursor.adapterName, _results[r]);
      }
    }
    if (callback) callback(error, _results);
  });
  return _results;
};

// (handler: (error?, data?: Document), callback?: ()))
// (handler: (error?, data?: Data), callback?: (), onlyData: Boolean = true)
Cursor.prototype.each = function(handler, callback, onlyData) {
  var cursor = this;
  this.adapter.cursorEach(this.pointer, function(error, data) {
    if (onlyData) handler(error, data);
    else handler(error, data?cursor.refs.documentsFactory.new(cursor.adapterName, data):undefined);
  }, function() {
    if( callback) callback();
  });
};

// (callback?: (error?, count?: Number = 0)) => count?: Number = 0
Cursor.prototype.count = function(callback) {
  var _count;
  this.adapter.cursorCount(this.pointer, function(error, count) {
    _count = count;
    if (callback) callback(error, _count);
  });
  return _count;
};

// (begin?: Number, length?: Number, callback?: (error?, results?: [Document])) => documents?: [Document]
// (begin?: Number, length?: Number, callback?: (error?, results?: [Data]), onlyData: Boolean = true) => documents?: [Data]
Cursor.prototype.slice = function(begin, length, callback, onlyData) {
  var cursor = this;
  var _results;
  this.adapter.cursorSlice(this.pointer, begin, length, function(error, results) {
    _results = results;
    if (!onlyData) {
      for (var r in _results) {
        _results[r] = cursor.refs.documentsFactory.new(cursor.adapterName, _results[r]);
      }
    }
    if (callback) callback(error, _results);
  });
  return _results;
};

// (callback?: (error?, result?: Document)) => result?: Document
// (callback?: (error?, result?: Data), onlyData: Boolean = true) => result?: Data
Cursor.prototype.first = function(callback, onlyData) {
  var cursor = this;
  var _result;
  this.adapter.cursorFirst(this.pointer, function(error, result) {
    _result = result;
    if (!onlyData) {
      _result = cursor.refs.documentsFactory.new(cursor.adapterName, result);
    }
    if (callback) callback(error, _result);
  }, onlyData);
  return _result;
};

Cursor.prototype.onCreated = function(handler) {
  var cursor = this;
  var observer = this.adapter.cursorOnCreated(this.pointer, function(newData, context) {
    handler.call(
      cursor,
      cursor.refs.documentsFactory.new(cursor.adapterName, newData),
      context
    );
  });
  return new Watcher(cursor, observer);
};

Cursor.prototype.onUpdated = function(handler) {
  var cursor = this;
  var observer = this.adapter.cursorOnUpdated(this.pointer, function(oldData, newData, context) {
    handler.call(
      cursor,
      cursor.refs.documentsFactory.new(cursor.adapterName, oldData),
      cursor.refs.documentsFactory.new(cursor.adapterName, newData),
      context
    );
  });
  return new Watcher(cursor, observer);
};

Cursor.prototype.onDeleted = function(handler) {
  var cursor = this;
  var observer = this.adapter.cursorOnDeleted(this.pointer, function(oldData, context) {
    handler.call(
      cursor,
      cursor.refs.documentsFactory.new(cursor.adapterName, oldData),
      context
    );
  });
  return new Watcher(cursor, observer);
};

/** Watcher **/

var Watcher = function(cursor, observer) {
  this.cursor = cursor;
  this.observer = observer;
};

Watcher.prototype.off = function() {
  this.cursor.adapter.cursorOff(this.observer);
};

exports.Cursor = Cursor;

/** adapterAutoComplete placeholders **/

// (adapter) => adapter|thrown Error
exports.adapterAutoComplete = function(adapter) {
  if (!adapter.cursorFetch) {
    if (adapter.cursorCount && adapter.cursorSlice) {
      adapter.cursorFetch = exports.fetchBySlice;
    } else if(adapter.cursorEach) {
      adapter.cursorFetch = exports.fetchByEach;
    } else throw new Error('Incompatible adapter!', 'You must define in adapter at least one of variants: cursorFetch or cursorEach or (cursorCount and cursorSlice)!');
  }
  if (!adapter.cursorEach) {
    if (adapter.cursorCount && adapter.cursorSlice) {
      adapter.cursorEach = exports.eachBySlice;
    } else if(adapter.cursorFetch) {
      adapter.cursorEach = exports.eachByFetch;
    } else throw new Error('Incompatible adapter!', 'You must define in adapter at least one of variants: cursorFetch or cursorEach or (cursorCount and cursorSlice)!');
  }
  if (!adapter.cursorCount) {
    adapter.cursorCount = exports.countByFetch;
  }
  if (!adapter.cursorSlice) {
    adapter.cursorSlice = exports.sliceByFetch;
  }
  if (!adapter.cursorFirst) {
    adapter.cursorFirst = exports.firstBySlice;
  }
  
  return adapter;
};

exports.fetchBySlice = function(pointer, callback) {
  var adapter = this;
  adapter.cursorCount(pointer, function(error, count) {
    if (error) callback(error);
    else adapter.cursorSlice(pointer, 0, count, callback);
  });
}

exports.fetchByEach = function(pointer, callback) {
  var _error;
  var results = [];
  var adapter = this;
  adapter.cursorEach(pointer, function(error, result) {
    if (!_error) {
      if (error) _error = error;
      results.push(result);
    }
  }, function() {
    callback(_error, _error?undefined:results);
  });
}

exports.eachBySlice = function(pointer, handler, callback) {
  var adapter = this;
  adapter.cursorCount(pointer, function(error, count) {
    if (error) callback(error);
    else {
      for (var c = 0; c < count; c++) {
        adapter.cursorSlice(pointer, c, 1, function(error, results) {
          handler(error, results?results[0]:undefined);
        });
      }
      callback();
    }
  });
}

exports.eachByFetch = function(pointer, handler, callback) {
  var adapter = this;
  adapter.cursorFetch(pointer, function(error, results) {
    if (error) callback(error);
    else {
      for (var c in results) {
        handler(error, results[c]);
      }
      callback();
    }
  });
}

exports.countByFetch = function(pointer, callback) {
  this.cursorFetch(pointer, function(error, results) {
    callback(error, results?results.length:undefined);
  });
}

exports.sliceByFetch = function(pointer, begin, length, callback) {
  this.cursorFetch(pointer, function(error, _results) {
    var results;
    if (_results) results = _results.slice(begin, begin+length);
    callback(error, results);
  });
}

exports.firstBySlice = function(pointer, callback) {
  this.cursorSlice(pointer, 0, 1, function(error, results) {
    callback(error, results?results[0]:undefined);
  });
}